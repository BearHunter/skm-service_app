package com.example.bearhunter.skmnservice;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.bearhunter.skmnservice.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    FusedLocationProviderClient fusedLocationProviderClient;

    GoogleMap mMap;
    LocationRequest locationRequest;
    LocationCallback locationCallback;
    Location location;
    LatLng latLng;
    Context context;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        Log.d("onCreate", "onCreate() 호출 됨");

        //권한 체크
        //권한 하나라도 없으면
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){

            Log.d("checkPermission", "requestPermissions 호출 함");

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    100);
        }
        //권한부여 성공 시
        else{
            Toast.makeText(this, "성공!!", Toast.LENGTH_SHORT).show();

            Log.d("Permission", "권한 부여 이미 됨");

            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

            getLocationCallback();
            getLocationRequest();
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());

            Log.d("requestLocationUpdates", "requestLocationUpdates() 호출 됨");

        }

        FragmentManager fragmentManager = getFragmentManager();
        MapFragment mapFragment = (MapFragment)fragmentManager
                .findFragmentById(R.id.fragment);
        mapFragment.getMapAsync(this);


        Log.d("mapFragment", "mapFragment 받음");

    }



    private void getLocationRequest() {

        Log.d("getLocationRequest", "getLocationRequest() 호출 됨");

        locationRequest = new LocationRequest();
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


    }

    private void getLocationCallback() {

        Log.d("getLocationCallback", "getLocationCallback() 호출 됨");

        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                Log.d("LocationRes", "onLocationResult() 호출 됨");

                //위치 값 얻어옴
                location = locationResult.getLastLocation();
                latLng = new LatLng(location.getLatitude(), location.getLongitude());

                Toast.makeText(getApplicationContext(), "위도 : " + latLng.latitude + " 경도 : " + latLng.longitude, Toast.LENGTH_LONG).show();
                Log.d("위도경도", "위도 : " + location.getLatitude() + "경도 : " + location.getLongitude());

                //지도 나타내기
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("현 위치");
                markerOptions.snippet("힘들다");

                mMap.addMarker(markerOptions);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17));



//                fusedLocationProviderClient.removeLocationUpdates(locationCallback);

            }


            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
            }
        };
    }


    //requestPermission 에 대한 결과 리턴
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //요청코드가 맞다면
        if(requestCode == 100){

            Log.d("요청 결과 리턴", "onRequestPermissionResult() 호출 돼서 요청코드랑 맞음");

            //허용을 받았다면
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "권한 허용 받음!!", Toast.LENGTH_SHORT).show();

            }
            //권한 거부 시
            else{
                Toast.makeText(this, "권한허용을 거부 하셨습니다. ㄲㅈ세요", Toast.LENGTH_SHORT).show();
                finish();
            }


        }else{
            Toast.makeText(this, "요청 코드가 맞지 않습니다", Toast.LENGTH_SHORT).show();
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }



    //지도 정보
    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.d("onMapReady", "onMapReady() 호출 됨");

        mMap = googleMap;



    }






}
