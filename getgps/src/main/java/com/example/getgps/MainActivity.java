package com.example.getgps;

import android.Manifest;
import android.app.FragmentManager;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    FusedLocationProviderClient fusedLocationProviderClient;

    GoogleMap mMap;
    LocationRequest locationRequest;
    LocationCallback locationCallback;
    Location location;
    LatLng latLng;
    Button button_start;
    Button button_end;
    Button button_cal;
    EditText editText_start_lat;
    EditText editText_start_lng;
    EditText editText_end_lat;
    EditText editText_end_lng;
    EditText editText_length;
    EditText editText_time;
    int[] click = {0, 0};
    long startTime, endTime;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_start = (Button)findViewById(R.id.button_start);
        button_end = (Button)findViewById(R.id.button_end);
        button_cal = (Button)findViewById(R.id.button_cal);

        editText_start_lat = (EditText)findViewById(R.id.edittext_start_lat);
        editText_start_lng = (EditText)findViewById(R.id.edittext_start_lng);
        editText_end_lat = (EditText)findViewById(R.id.edittext_end_lat);
        editText_end_lng = (EditText)findViewById(R.id.edittext_end_lng);
        editText_length = (EditText)findViewById(R.id.edittext_length);
        editText_time = (EditText)findViewById(R.id.edittext_time);

        button_start.setOnClickListener(new View.OnClickListener() {
            @Override
            @SuppressWarnings("MissingPermission")
            public void onClick(View v) {

                click[0] = 1;
                fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                Log.d("1", "시작 button에서 requestLocationUpdate()");
            }
        });


        button_end.setOnClickListener(new View.OnClickListener() {
            @Override
            @SuppressWarnings("MissingPermission")
            public void onClick(View v) {

                click[1] = 1;
                fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                Log.d("1", "도착 button에서 requestLocationUpdate()");

            }
        });


        button_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //거리 계산
                Double resultMeter = MyCalDistance.calDistance(Double.parseDouble(editText_start_lat.getText().toString()),
                        Double.parseDouble(editText_start_lng.getText().toString()),
                        Double.parseDouble(editText_end_lat.getText().toString()),
                        Double.parseDouble(editText_end_lng.getText().toString()));

                editText_length.setText(String.format(Locale.US, "%.6f m",resultMeter));

                //시간 계산
                double diff = ((double)(endTime - startTime) / 1000);
                editText_time.setText(String.format(Locale.US, "%.2f 초", diff));
            }
        });



        //권한 있는 경우
        if(MyPermission.MycheckPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                || MyPermission.MycheckPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(this, "성공!!", Toast.LENGTH_SHORT).show();

            Log.d("Permission", "권한 부여 이미 됨");

            //GPS 실행
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            getLocationCallback();
            getLocationRequest();


//            fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
//            Log.d("requestLocationUpdates", "requestLocationUpdates() 호출 됨");

        }
        //권한 없는 경우
        else{
            MyPermission.MyrequestPermissionResult(this);
        }

        //Map Fragment
        FragmentManager fragmentManager = getFragmentManager();
        MapFragment mapFragment = (MapFragment)fragmentManager
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Log.d("mapFragment", "mapFragment 받음");
    }

    private void getLocationRequest() {

        Log.d("getLocationRequest", "getLocationRequest() 호출 됨");

        locationRequest = new LocationRequest();
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void getLocationCallback() {

        Log.d("getLocationCallback", "getLocationCallback() 호출 됨");

        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                Log.d("LocationRes", "onLocationResult() 호출 됨");

                //위치 값 얻어옴
                location = locationResult.getLastLocation();
                latLng = new LatLng(location.getLatitude(), location.getLongitude());

                Toast.makeText(getApplicationContext(), "위도 : " + latLng.latitude + " 경도 : " + latLng.longitude, Toast.LENGTH_LONG).show();
                Log.d("위도경도", "위도 : " + location.getLatitude() + "경도 : " + location.getLongitude());

                if(click[0] == 1){
                    startTime = System.currentTimeMillis();
                    editText_start_lat.setText(String.valueOf(latLng.latitude));
                    editText_start_lng.setText(String.valueOf(latLng.longitude));
                    click[0] = 0;
                }else if(click[1] == 1){
                    endTime = System.currentTimeMillis();
                    editText_end_lat.setText(String.valueOf(latLng.latitude));
                    editText_end_lng.setText(String.valueOf(latLng.longitude));
                    click[1] = 0;
                }else{
                    Log.d("Fail", "EditText에 담기 실패");
                }

                //지도 나타내기
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("현 위치");
                markerOptions.snippet("윾");

                mMap.addMarker(markerOptions);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17));

                //1번만 받아오기
                fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                Log.d("removeUpdate", "removeLocationUpdates()댐");

            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
            }
        };
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.d("onMapReady", "onMapReady() 호출 됨");

        mMap = googleMap;
        LatLng tempLatLng = new LatLng(37.293988, 126.975156);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(tempLatLng));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(17));
    }


    //requestPermission 에 대한 결과 리턴
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //요청코드가 맞다면
        if(requestCode == 200){

            Log.d("요청 결과 리턴", "onRequestPermissionResult() 호출 돼서 요청코드랑 맞음");

            //허용을 받았다면
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "권한 허용 받음!!", Toast.LENGTH_SHORT).show();

                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

                getLocationCallback();
                getLocationRequest();

//                fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
//                Log.d("requestLocationUpdates", "requestLocationUpdates() result 에서 호출 됨");
            }
            //권한 거부 시
            else{
                Toast.makeText(this, "권한허용을 거부 하셨습니다. 가세요", Toast.LENGTH_SHORT).show();
                finish();
            }

        }else{
            Toast.makeText(this, "요청 코드가 맞지 않습니다", Toast.LENGTH_SHORT).show();
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
