package com.example.getgps;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

public class MyPermission {

    //권한 부여 여부 메서드
    public static boolean MycheckPermission(Activity activity, String permission){
        int permissionResult = ActivityCompat.checkSelfPermission(activity, permission);
        if(permissionResult == PackageManager.PERMISSION_GRANTED)
            return true;
        else
            return false;
    }


    //권한 요청 메서드
    public static void MyrequestPermissionResult(Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                200);
        Log.d("checkPermission", "checkSelfPermission 함");
    }


}
